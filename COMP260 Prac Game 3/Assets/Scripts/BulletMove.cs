﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

	//separate speed and direction so we can
	//tune the speed without changing the code
	public float speed = 10.0f;
	public Vector3 direction;
	private Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		
	}

	void FixedUpdate(){
		rb.velocity = speed * direction;

	}

	void OnCollisionEnter(Collision collision){
		//destroy the bullet
		Destroy(gameObject);
	}
	// Update is called once per frame

}
