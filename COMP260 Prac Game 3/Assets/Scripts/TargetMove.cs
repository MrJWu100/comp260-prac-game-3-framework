﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour {

	public float startTime = 0.0f;
	public Animator animator;

	
	// Update is called once per frame
	void Update () {
		//set the Start parameter to true
		//if we have passed the start time
		if(Time.time >= startTime){
			animator.SetTrigger ("Start");
		}
	}

	void OnCollisionEnter(Collision collision){
		if (collision.collider.name == "Bullet") {
			//transition to the Die animation
			animator.SetTrigger ("Hit");
		}
	}

	public void DestroySphere(){
		Destroy (gameObject);
	}
}
